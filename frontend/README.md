## 개발환경

|  구분        |  설명                                                 |
|--------------|-------------------------------------------------------|
| Angular     | 11.2.6   |
| Angular CLI    | 11.2.10   |
| NodeJS       | 14.15.1 (LTS)                                        |
| NPM          | 6.14.8                                               |



## vscode
 * ctrl+shift+` : terminal > new terminal 터미널 창 열기
 * 새로 열린 창에 상단 콤보박스에 `powershell` 이라고 뜨는 경우 `select default shell` 을 `cmd`창으로 선택 후 창을 닫고 다시 터미널 창 열기
```
$ npm i
~~~~
~~~~
how to change this setting, see http://angular.io/analytics. //엔터
```
* 왼쪽 하단 `NPM SCRIPT` 창 오픈 > start 항목 삼각형 클릭
* 크롬 브라우져 오픈후 주소로 이동
```
http://localhost:8080
```

## IntelliJ
 * add configuration
    - build / run
      - "Add Configuration" > "+" > Npm 선택 > Command: `run` > Script: `start` > run 실행  
        ![run](../screenshot/configuration_run.png)  
    - debug
      - "Add Configuration" > "+" > Javascript Debug 선택 > URL: `http://localhost:8080` 입력 > debug 실행  
        ![debug](../screenshot/configuration_debug.png)   


## tip and tricky
 1. vscode extention 추가 방법: 왼쪽에서 블럭 모양 아이콘 클릭후 angular 검색하여 추가
  - `Angular Language Service`
  - `Angular Schematics`
  - `Debugger for Chrome` * 디버깅 필수 플러그인
 2. 콘솔로그 안찍고 디버깅 방법
  - `F5` 클릭 `launch.json` 파일내 아래 내용 입력 후 저장
```json
{
  // Use IntelliSense to learn about possible attributes.
  // Hover to view descriptions of existing attributes.
  // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
  "version": "0.2.0",
  "configurations": [
      {
          "type": "chrome",
          "request": "launch",
          "name": "Launch Chrome against localhost",
          "url": "http://localhost:4200",
          "sourceMaps": true,
          "webRoot": "${workspaceFolder}",
      }
  ]
}
```
  - 소스에서 추적하고 싶은 라인에 브레이크를 걸고
  - 왼쪽 하단 `NPM SCRIPT` 창 오픈 > `start` 항목 삼각형 클릭
  - `F5` 키 입력 후 왼쪽 `VALITABLES` 과, `WATCH` 창을 이용하여 값을 확인한다.
 3. json을 클래스로 변환
```
http://json2ts.com/
```

## 프로젝트 구성(구현)
### 참고모델  
 [`Angular architecture best practices`](https://dev-academy.com/angular-architecture-best-practices/) 아카데미 블로그 참고.  
 - 모듈내부 아키텍처  
   ![](https://dev-academy.com/angular-architecture-best-practices/flow%20abstract.gif)  
 
 - 모듈간 관계  
   ![](https://dev-academy.com/angular-architecture-best-practices/large_imports.webp)  
 
### 폴더구조

  ```
 ├── app
 │   ├── app-routing.module.ts
 │   ├── app.component.html
 │   ├── app.component.scss
 │   ├── app.component.spec.ts
 │   ├── app.component.ts
 │   ├── app.module.ts
 │   ├── core           // 공통으로 사용하는 모델로 '싱글'인스턴스로 사용되는 것들
 │   │   ├── component  // 
 │   │   │   └── single.component.ts
 │   │   ├── core.api.ts
 │   │   └── core.module.ts
 │   ├── shared         // 공통으로 사용되는 모델로 '서비스'모델별 인스턴스 
 │   │   ├── component
 │   │   │   └── toolbar
 │   │   │       ├── toolbar.component.html
 │   │   │       ├── toolbar.component.scss
 │   │   │       ├── toolbar.component.spec.ts
 │   │   │       └── toolbar.component.ts
 │   │   └── shared.module.ts
 │   └── webhook    // core, shared를 제외한 기타 모듈들 -- 서비스 단위로 구분.
 │       ├── container
 │       │   ├── intro
 │       │   │   ├── intro.component.html
 │       │   │   ├── intro.component.scss
 │       │   │   ├── intro.component.spec.ts
 │       │   │   └── intro.component.ts
 │       │   └── main
 │       │       ├── main.component.html
 │       │       ├── main.component.scss
 │       │       ├── main.component.spec.ts
 │       │       └── main.component.ts
 │       ├── webhook.api.ts
 │       ├── webhook.facade.ts
 │       ├── webhook.interceptor.ts
 │       ├── webhook.model.ts
 │       ├── webhook.module.ts
 │       ├── webhook.routing.module.ts
 │       └── webhook.state.ts
 ├── assets
 ├── environments
 │   ├── environment.prod.ts
 │   └── environment.ts
 ├── favicon.ico
 ├── index.html
 ├── main.ts
 ├── polyfills.ts
 ├── styles.scss
 └── test.ts

  ```

---

## 브랜치 규칙

- 브랜치 종류
  - master: 메인 유지관리 브랜치 릴리즈 시 태깅추가, 운영환경
  - develop: 실행 가능한, 에러 없는 최신 소스, 개발자 공유
  - feature/{feature name} : 개발항목 구

- 브랜치 [그래프][1] 예)

  ```
               hotfix/QA_#11---
               /                \
  -  master ------------------------------(merge & taging)
         \                                     /
	       develop ------------------------(merge)-----
	          \                            /
               --- feature/m-second(localPC)------

  ```
 - 개발 작업 순서
 1. local PC에 develop 에서 소스를 당겨 온다. (clone)
 2. feature 항목 branch를 만든다. (예. feature/m-first, feature/card)
 3. 개발 작업 수행
 4. 로컬에 commit 한다.
 5. 원격에 push 한다.
 6. Merge Request를 생성한다. (리뷰요청)
 7. 리뷰를 기다린다. (리뷰어는 리뷰 후 머지 한다.)
 7-1. 컨플릭이 발생한 경우 개발담당자가 수정 후 Push 한다.
 8. 머지가 완료된 후 코멘트로 Develop 브랜치가 업데이트 된 것을 알린다.

 - Feature브랜치를 중심을 수시로 develop 브랜치를 당겨서 머지해야 나중에 충돌해결하는데 어려움이 없다.


[1]:https://gitlab.com/jckoh7/node-basic/-/network/master
