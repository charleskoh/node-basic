import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import { CoreState } from '../core/core.state';

@Injectable()
export class SharedState extends CoreState {

  constructor() {
      super();
  }
}
