import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './component/toolbar/toolbar.component';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModalComponent } from './component/common-modal/common-modal.component';
import { NaverMapComponent } from './component/naver-map/naver-map.component';
import { HeaderComponent } from './component/header/header.component';
import { IndicatorComponent } from './component/indicator/indicator.component';
import { ListTabComponent } from './component/list-tab/list-tab.component';
import { NotFoundComponent } from './container/not-found/not-found.component';
import { SharedApi } from './shared.api';
import { SharedFacade } from './shared.facade';
import { SharedGuard } from './shared.guard';
import { AuthFacade } from '../module/auth/auth.facade';
import { AuthApi } from '../module/auth/auth.api';
import { AuthState } from '../module/auth/auth.state';



@NgModule({
  declarations: [
    ToolbarComponent, 
    CommonModalComponent, 
    NaverMapComponent, 
    HeaderComponent, 
    IndicatorComponent, 
    ListTabComponent, NotFoundComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ToolbarComponent,
    CommonModalComponent,
    NaverMapComponent, 
    HeaderComponent, 
    IndicatorComponent, 
    ListTabComponent,
  ],
  providers: [
    AuthApi,
    AuthState,
    AuthFacade,
  ]
})
export class SharedModule { 
  static forRoot() {
    return {
      ngModule: SharedModule,
      providers: [
        SharedApi,
        SharedFacade,
        SharedGuard,
      ]
    };
  }
}
