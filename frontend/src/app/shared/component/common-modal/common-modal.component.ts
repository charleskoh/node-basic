import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mcr-common-modal',
  templateUrl: './common-modal.component.html',
  styleUrls: ['./common-modal.component.scss']
})
export class CommonModalComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
