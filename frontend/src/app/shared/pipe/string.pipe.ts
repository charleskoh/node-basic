import {Pipe, PipeTransform} from '@angular/core';
import util from '@util/common.util';

@Pipe({
  name:'withcomma',
})
export class WithcommaPipePipe implements PipeTransform {
  transform(value: any, ...args): any {
    return util.getNumberWithComma(value);
  }
}
