import {Injectable} from '@angular/core';
import {first} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { CoreFacade } from '../core/core.facade';
import { SharedApi } from './shared.api';
import { SharedState } from './shared.state';

@Injectable()
export class SharedFacade extends CoreFacade {

  constructor(private api: SharedApi, protected state: SharedState) {
      super(state);
  }

}
