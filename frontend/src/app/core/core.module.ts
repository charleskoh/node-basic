import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreHttp } from './core.http';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [

  ]
})
export class CoreModule { 
  static forRoot() {
    return {
      ngModule: CoreModule,
      providers: [
        CoreHttp,
        {provide: LOCALE_ID, useValue: 'ko'},
      ]
    }
  }
}
