import {HttpClient} from '@angular/common/http';
import {CommonResponse, CommonResponseAdapter} from './model/common-response-model';
import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

export const throttle = (fn: ((...args: any[]) => void), wait: number = 400) => {
  let inThrottle = false;
  let lastFn: ReturnType<typeof setTimeout> = null;
  let lastTime: number;

  return (obj: any) => {
    const context = obj;
    if (!inThrottle) {
      fn.apply(context, fn.arguments);
      lastTime = Date.now();
      inThrottle = true;
    } else {
      clearTimeout(lastFn);
      lastFn = setTimeout(() => {
        if (Date.now() - lastTime >= wait) {
          fn.apply(context, fn.arguments);
          lastTime = Date.now();
        }
      }, Math.max(wait - (Date.now() - lastTime), 0));
    }
  };
};

export const retry = (fn: ((...args: any[]) => void), count: number = 3) => {
  return (obj: any) => {
    fn.apply(obj, fn.arguments);
  };
};

@Injectable()
export class CoreHttp {

  constructor(private http: HttpClient, private adapter: CommonResponseAdapter) {
  }

  request(method: string, url: string, param?: object, option?: object): Observable<CommonResponse> {
    const $this = this;
    switch (method?.toLowerCase()) {
      case 'get':
        return this.http.get<CommonResponse>(url, option)
          .pipe(map( res => {
            return $this.adapter.adapt(res);
          }));
        break;
      case 'patch':
        return this.http.patch<CommonResponse>(url, param, option)
          .pipe(map( res => {
            return $this.adapter.adapt(res);
          }));
        break;
      case 'delete':
        return this.http.delete<CommonResponse>(url, param)
          .pipe(map( res => {
            return $this.adapter.adapt(res);
          }));
        break;
      default:
        return this.http.post<CommonResponse>(url, param, option)
          .pipe(map( res => {
            return $this.adapter.adapt(res);
          }));
        break;
    }
  }
}

// tslint:disable-next-line:no-namespace
export namespace CommonApi {
  const HTTP_REQ_METHOD = {
    GET: 'GET',
    POST: 'POST',
    DEL: 'DELETE',
    UPDATE: 'UPDATE',
    CREATE: 'CREATE',
  } as const;
}
