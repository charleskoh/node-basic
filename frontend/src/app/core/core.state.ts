import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import { environment } from 'src/environments/environment';
import util from 'src/app/util/common.util';
import { USER_DEFAULT } from './core.const';

export class CoreState {

  constructor() {
  }

  private static fromSessionStorage(key: string): any {
    return JSON.parse(sessionStorage.getItem(key));
  }

  getState<T>(key: string): Observable<T> {
    return this[key].asObservable();
  }
  setState<T>(key: string, state: T): void {
    if (key.match(/^resv*/) || key === 'token') {
      sessionStorage.setItem(key, JSON.stringify(state));
    }
    this[key].next(state);
  }
  getValue<T>(key: string): T {
    if (key.match(/^resv*/) || key === 'token') {
      if (environment.production && !util.isWebview()) {
        return this[key].getValue() || window.sessionStorage.getItem(USER_DEFAULT.TOKEN);
      } else {
        return this[key].getValue() || window.localStorage.getItem(USER_DEFAULT.TOKEN);
      }
    }
    return this[key].getValue();
  }

}
