import {Injectable} from '@angular/core';
import {first} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { CoreState } from './core.state';

export class CoreFacade {

  constructor(protected state: CoreState) {
  }

  getState<T>(key): Observable<T> {
    return this.state.getState<T>(key);
  }

  setState<T>(key, state: T): void {
    this.state.setState<T>(key, state);
  }

  getStateValue<T>(key): T {
    return this.state.getValue<T>(key);
  }
}
