import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { SigninComponent } from './container/signin/signin.component';
import { SignupComponent } from './container/signup/signup.component';

const routes: Routes = [
    { path: 'auth/signin', component: SigninComponent, canActivate: [AuthGuard] },
    { path: 'auth/signup', component: SignupComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
      AuthGuard,
  ]
})
export class AuthRoutingModule { }
