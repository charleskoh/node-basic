import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import { CoreState } from 'src/app/core/core.state';

@Injectable()
export class AuthState extends CoreState {
  token: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  
  constructor() {
      super();
  }
}
