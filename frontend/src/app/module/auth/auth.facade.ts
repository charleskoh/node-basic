import {Injectable} from '@angular/core';
import {first} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { CoreFacade } from 'src/app/core/core.facade';
import { AuthState } from './auth.state';
import { AuthApi } from './auth.api';

@Injectable()
export class AuthFacade extends CoreFacade {

  constructor(private api: AuthApi, protected state: AuthState) {
      super(state);
  }

  getToken(): string {
      return this.state.getValue<string>('token');
  }
  
  setToken(token): void {
      this.state.setState<string>('token', token);
  }
}
