import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import { AuthState } from './auth.state';
import { AuthFacade } from './auth.facade';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private facade: AuthFacade,
    private authState: AuthState
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    // if (environment.production && !util.isWebview()) {
    //   setTimeout(() => {
    //     window.location.href = 'https://www.macaront.com';
    //   }, 700);
    //   return false;
    // }

    if (this.facade.getToken() == null) {
      return true;
    }
    
    const token = next.queryParams['token'];
    if (token == null) {
      return true;
    }

    this.facade.setToken(token);
    this.router.navigate(['/home/main']);
    return false;
  }
}
