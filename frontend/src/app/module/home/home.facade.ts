import {Injectable} from '@angular/core';
import {first} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { CoreFacade } from 'src/app/core/core.facade';
import { HomeApi } from './home.api';
import { HomeState } from './home.state';

@Injectable()
export class HomeFacade extends CoreFacade {

  constructor(private api: HomeApi, protected state: HomeState) {
      super(state);
  }

}
