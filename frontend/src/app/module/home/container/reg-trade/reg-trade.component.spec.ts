import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegTradeComponent } from './reg-trade.component';

describe('RegTradeComponent', () => {
  let component: RegTradeComponent;
  let fixture: ComponentFixture<RegTradeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegTradeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegTradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
