import express from "express";
import authRouter from "../service/auth/router";
import wineRouter from "../service/wine/router";
import purchaseRouter from "../service/purchase/router";
import reviewRouter from "../service/review/router";
import storeRouter from "../service/store/router";

const router = express.Router();

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

router.use('/v1/auth', authRouter);
router.use('/v1/wine', wineRouter);
router.use('/v1/purchase', purchaseRouter);
router.use('/v1/review', reviewRouter);
router.use('/v1/store', storeRouter);

export default router