

export interface ITokenPolicy {
    applyPolicyAccessToken(payload: any): any;    
    applyPolicyRefreshToken(payload: any): any;    
}

export class KSTTokenPolicy {
    applyPolicyAccessToken(payload: any): any {
        const policy = Object.assign(payload, {
            iss: 'KST Mobility',
            sub: 'Use to access API for the Service',
            aud: payload?.aud || '<user id>',
            exp: Math.floor(Date.now() / 1000) + 8640,  // 60 * 60 * 24 second
            // iat: Math.floor(Date.now() / 1000)  // jwt 라이브러리에서 자동 생성
        });
        return policy;
    }

    applyPolicyRefreshToken(payload: any): any {
        const policy = Object.assign(payload, {
            iss: 'KST Mobility',
            sub: 'Use to access API for the Service',
            aud: payload?.aud || '<user id>',
            exp: Math.floor(Date.now() / 1000) + 777600,  // (60 * 60 * 24 * 90) == 90 days
            // iat: Math.floor(Date.now() / 1000)
        });
        return policy;
    }
}