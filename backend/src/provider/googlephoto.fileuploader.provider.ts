import IFileUploader from './fileuploader.provider';

const fs = require('fs');
const libpath = require('path');
const { GPhotos } = require('upload-gphotos');
const util = require('util');
const gphotos = new GPhotos();
const readFile = util.promisify(fs.readFile);

class GooglePhotoUploader implements IFileUploader {
    constructor() {

    }

    async upload(formData, albumName, size) {
        const content = await readFile(__dirname + '/credentials.json', 'utf8');
        const {username, password} = JSON.parse(content);
        await gphotos.signin({
            username,
            password,
        });

        const stream = fs.createReadStream(formData.tempFilePath);

        const album = await gphotos.searchAlbum({ title: albumName });
        
        const photo = await gphotos.upload({
            stream: stream,
            size: size,
            filename: formData.md5,
        });
        await album.append(photo);
        
        return photo;
    }
}

export default new GooglePhotoUploader()