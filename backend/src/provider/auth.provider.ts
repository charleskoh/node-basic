

export default interface IAuthProvider {
    signin(q: any): Promise<any>;
    signup(q: any): Promise<any>;
    signout(q: any): Promise<any>;
    auth(q: any): Promise<any>;
    confirm(q: any): Promise<any>;
    onAuthStatus(callback:(info: any) => void): void;
}