import { HTTP400Error } from "../../middleware/error.handler";
import has from 'has-keys';

import model from "./model";
import {IStore} from "./model";

export default {
    async getStoreById(req, res, next) {
        if (!has(req.params, 'id')) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let query = {c_id: req.params['id']};
        const page = Number(req.query['page']) || 0;
        const size = Number(req.query['size']) || 20;

        let data = await model.store.getAll(page, size, query);
        if (!data) {
            res.json({ status: true, message: 'success', data: [] });
            return;
        }

        res.json({ status: true, message: 'success', data });
    },

    async writeStore(req, res, next) {

        if (!has(req.body, ['name', 'addr', 'lat', 'lng'])) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let store = {
            name: req.body['name'],
            addr: req.body['addr'],
            lat: req.body['lat'],
            lng: req.body['lng'],
            start: req.body['start'],
            closed: req.body['closed'],
            sales_cnt: req.body['sales_cnt'],
            review_point: req.body['review_point'],
            c_dt: Date.now(),
            m_dt: Date.now(),
            c_id: req.user.aud,
            m_id: req.user.aud,
        }
        let info = await model.store.create(store);

        res.json({ status: true, message: 'success', data: {_id: info._id.toString()} } );
    },
    
    async deleteStore(req, res, next) {
        if (!Array.isArray(req.body)) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let validElements = req.body.filter( element => {
            return has(element, ['_id']);
        });

        if (validElements.length == 0) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        var data = [];
        for (const element of validElements) {
            let count = await model.store.delete(element._id);
            if (count > 0) {
                data.push(element._id);
            }
        }

        res.json({ status: true, message: 'success', data: data});
    },

    async updateStore(req, res, next) {

        if (!has(req.params, ['id'])) {
            res.json({status: false, message: 'Bad request'});
            return;
        }

        var store = await model.store.getById(req.params['id']) as IStore;
        if (store) {
            store.name = req.params['name'] || store.name;
            store.addr = req.params['addr'] || store.addr;
            store.start = req.params['start'] || store.start;
            store.finish = req.params['finish'] || store.finish;
            store.closed = req.params['closed'] || store.closed;
            store.lat = req.params['lat'] || store.lat;
            store.lng = req.params['lng'] || store.lng;
            store.sales_cnt = req.params['sales_cnt'] || store.sales_cnt;
            store.review_point = req.params['review_point'] || store.review_point;
            store.m_dt = Date.now();
            store.m_id = req.user.aud;
        } else {
            res.json({status: false, message: 'Not found'});
            return ;
        }

        let data = await model.store.update(req.params['id'], store);

        res.json({ status: true, message: 'success', data: data});
    },

    async searchFromGPS(req, res, next) {

        if (!has(req.query, ['lat', 'lng'])) {
            res.json({status: false, message: 'Bad request'});
            return;
        }
        const page = Number(req.query['page']) || 0;
        const size = Number(req.query['size']) || 20;

        let gt_lat = Number(req.query['lat']) - 0.00005;
        let gt_lng = Number(req.query['lng']) - 0.00005;
        let lt_lat = Number(req.query['lat']) + 0.00005;
        let lt_lng = Number(req.query['lng']) + 0.00005;
    
        let query = {lat: {$gt: gt_lat, $lt: lt_lat}, lng: {$gt: gt_lng, $lt: lt_lng}};
        /**
         * $where는 free tier에서는 사용할 수 없음.. ㅡ.ㅡ; M5 윗버전부터 가능.  
         */
        // let _lat = Number(req.query['lat']).toFixed(4);
        // let _lng = Number(req.query['lng']).toFixed(4);
        // let query = {$where: {
        //     function () {
        //         return this.lat.toFixed(4) === _lat && this.lng.toFixed(4) === _lng;
        //     }
        // }};

        
        let data = await model.store.getAll(page, size, query);
        if (!data) {
            res.json({ status: true, message: 'success', data: [] });
            return;
        }

        res.json({ status: true, message: 'success', data });
    }
}