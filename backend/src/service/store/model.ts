import MongoDBProvider from "../../provider/mongo.database.provider";

export interface IStore {
    _id: string;
    name: string;
    addr: string;
    start: string;
    finish: string;
    closed: string[];
    lat: number;
    lng: number;
    sales_cnt: number;
    review_point: number;
    c_dt: number;
    m_dt: number;
    c_id: string;
    m_id: string;
}

function bindCollection<T>(db: MongoDBProvider) {
    return {
        async getAll(page = 0, size = 20, query) {
            let result = await db.find<T>(page, size, query);
            return result;
        },
        
        async getById(id) {
            let result = await db.find<T>(0, 20, {"_id": id});
            return result.length > 0 ? result[0] : null;
        },

        async create(store) {
            return await db.insertOne<T>(store)
        },

        async update(id, store) {
            return await db.findOneAndUpdate<T>({"_id": id}, {$set: store}, { returnNewDocument: true });
        },

        async updateWithPipe(id, pipe) {
            return await db.findOneAndUpdate<T>({"_id": id}, pipe, { returnNewDocument: true });
        },

        async delete(id) {
            return await db.deleteOne({_id: id});
        }
    };
};

const store = bindCollection<IStore>(new MongoDBProvider('WineQuery/Store'))

export default {
    store,
};