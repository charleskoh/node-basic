
import FirebaseRealTimeDBProvider from "../../provider/firebase.database.provider";
import {ObjectId} from "mongodb"
import { isObjectBindingPattern } from 'typescript';
import MongoDBProvider from "../../provider/mongo.database.provider";

// const dbo = new FirebaseRealTimeDBProvider('webhook');

export interface IWine {
    _id: string;
    authCookie: string;
    type: string;
    name: string;
    en_name: string;
    vintage: string;
    signature: string;
    review_cnt: number;
    sales_cnt: number;
    c_dt: string;
    m_dt: string;
    c_id: string;
}

export interface IWineType {
    _id: string;
    name: string;
    c_dt: string;
    m_dt: string;
    c_id: string;
}

function bindCollection<T>(db: MongoDBProvider) { 
    return {
        async getAll(page = 0, size = 20, query) {
            let result = await db.find<T>(page, size, query);
            return result;
        },
        
        async getById(id) {
            let result = await db.find<T>(0, 20, {"_id": id});
            return result.length > 0 ? result[0] : {};
        },

        async create(wine) {
            return await db.insertOne<T>(wine)
        },

        async update(id, wine) {
            return await db.findOneAndUpdate<T>({"_id": id}, {$set: wine}, { returnNewDocument: true });
        },

        async updateWithPipe(id, pipe) {
            return await db.findOneAndUpdate<T>({"_id": id}, pipe, { returnNewDocument: true });
        },

        async delete(id) {
            return await db.deleteOne({_id: id});
        }
    };
};

const wine = bindCollection<IWine>(new MongoDBProvider('WineQuery/Wine'))
const wineType = bindCollection<IWineType>(new MongoDBProvider('WineQuery/WineType'))

export default {
    wine: wine,
    wineType: wineType,
};