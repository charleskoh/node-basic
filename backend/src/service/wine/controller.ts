import { HTTP400Error } from "../../middleware/error.handler";
import uploader from '../../provider/googlephoto.fileuploader.provider';
import has from 'has-keys';

import wineModel from "./model";
import photoModel from '../photo/model';

export default {

    async getWine(req, res, next) {
        var query = {}
        if (has(req.query, 'name')) {
            query['$or'] = [];
            query['$or'].push({name: {$regex: new RegExp(req.query['name'], 'g')}});
            query['$or'].push({en_name: {$regex: new RegExp(req.query['name'], 'g')}});
        }
        if (has(req.query, 'type')) {
            query['type'] = {$regex: new RegExp(req.query['type'], 'g')};
        }
        if (has(req.query, 'signature')) {
            query['signature'] = {$regex: new RegExp(req.query['signature'], 'g')};
        }

        const page = Number(req.query['page']) || 0;
        const size = Number(req.query['size']) || 20;

        let data = await wineModel.wine.getAll(page, size, query);
        if (!data) {
            res.json({ status: true, message: 'success', data: [] });
            return;
        }
        res.json({ status: true, message: 'success', data });
    },
    
    async getWineById(req, res, next) {
        if (!has(req.params, 'id')) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let {id} = req.params;
        let data = await wineModel.wine.getById(id);
        if (!data) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }
        
        res.json({ status: true, message: 'success', data });
    },

    async writeWine(req, res, next) {
        if (!Array.isArray(req.body)) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let validElements = req.body.filter( element => {
            return has(element, ['name', 'type', 'vintage', 'c_id']);
        });

        if (validElements.length == 0) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        var data = [];
        try {
            for (const element of validElements ) {
                var item =  {
                    name: element.name,
                    type: element.type,
                    vintage: element.vintage,
                    c_id: req.user.aud,
                    m_id: req.user.aud, 
                    c_dt: Date.now(),
                    m_dt: Date.now(),
                }
                let wine = await wineModel.wine.create(item);
                data.push({_id: wine._id.toString()});
            }
        } catch (e) {
            if (data.length == 0) {
                res.json({ status: false, message: 'Failed to Create'});
                return;
            }
        }
        res.json({ status: true, message: 'success', data } );
    },
    async deleteWine(req, res, next) {
        if (!Array.isArray(req.body)) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let validElements = req.body.filter( element => {
            return has(element, ['_id']);
        });

        if (validElements.length == 0) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        var data = [];
        for (const element of validElements) {
            let count = await wineModel.wine.delete(element._id);
            if (count) {
                data.push(element._id);
            }
        }

        res.json({ status: true, message: 'success', data: data});
    },

    async uploadWinePhoto(req, res, next) {
        if (!has(req.files, ['winephoto']) && !req.body['album']) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        const userid = req.user.aud;
        const photo = await uploader.upload(req.files.winephoto, req.body['album'], req.files.winephoto.size);
        if (photo) {
            var item =  {
                gphoto_id: photo.info.id,
                rawUrl: photo.info.rawUrl,
                width: photo.info.width,
                height: photo.info.height,
                c_id: userid,
                m_id: userid, 
                c_dt: Date.now(),
                m_dt: Date.now(),
            }
            const savedPhoto = await photoModel.photo.create(item);
            res.json({status: true, message: 'success', data: {photo_id: savedPhoto._id.toString(), url: photo.info.rawUrl, width: photo.info.width, height: photo.info.height}});
        } else {
            res.json({status: false, message: 'failed to upload photo'});
        }
    },
 }