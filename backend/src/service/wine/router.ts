import express from "express";
import { config } from "../../middleware/config";
// import validate from 'express-validation';
import controll from "./controller"
// import validation from "./validation"
const expressJwt = require('express-jwt');

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Wine
 *   description: Wine APIs
 */

/**
 * @swagger
 * definitions:
 *   Wine:
 *     type: 'object'
 *     properties: 
 *       _id:
 *         type: 'string'
 *       name:
 *         type: 'string'
 *       en_name:
 *         type: 'string'
 *       type:
 *         type: 'string'
 *       vintage:
 *         type: 'string'
 *       signature:
 *         type: 'string'
 *       review_cnt:
 *         type: 'number'
 *       sales_cnt:
 *         type: 'number'
 *       c_dt:
 *         type: 'number'
 *       m_dt:
 *         type: 'number'
 *       c_id:
 *         type: 'string'
 *       m_id:
 *         type: 'string'
 */

/**
 * @openapi
 * paths:
 *   /api/v1/wine/read:
 *     get:
 *       summary: 와인 조회
 *       tags: [Wine]
 *       parameters:
 *         - in: query
 *           name: name
 *           schema:
 *             type: string
 *           description: '와인 명'
 *         - in: query
 *           name: type
 *           schema:
 *             type: string
 *           description: '와인 타입(레드/화이트)'
 *         - in: query
 *           name: signature
 *           schema:
 *             type: string
 *           description: '와인 특징'
 *         - in: query
 *           name: page
 *           schema:
 *             type: integer
 *           description: '페이지 번호: 0 부터'
 *         - in: query
 *           name: size
 *           schema:
 *             type: integer
 *           description: '페이지 사이즈'
 *       responses:
 *         200:
 *           description: 와인 조회 성공
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: boolean
 *               message:
 *                 type: string
 *               data:
 *                 type: array
 *                 items:
 *                   $ref: '#/definitions/Wine'
 */ 
router.route('/read').get(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.getWine);

/**
 * @swagger
 * paths:
 *   /api/v1/wine/read/{id}:
 *     get:
 *       summary: 와인 단건 조회
 *       tags: [Wine]
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: string
 *           description: '와인 아이디'
 *       responses:
 *         200:
 *           description: 와인 조회 성공
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: boolean
 *               message:
 *                 type: string
 *               data:
 *                 $ref: '#/definitions/Wine'
 */ 
router.route('/read/:id').get(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.getWineById);

/**
 * @openapi
 * paths:
 *   /api/v1/wine/write:
 *     post:
 *       summary: 와인 저장
 *       tags: [Wine]
 *       requestBody:
 *         content:
 *           application/json: 
 *             name: Wine
 *             description: 'Wine array'
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/definitions/Wine'
 *             examples:
 *               SampleWine1:
 *                 value:
 *                   [ 
 *                     {
 *                       name: '몬테스 타이타',
 *                       en_name: 'Montes Taita',
 *                       type: '레드',
 *                       vintage: '3 years',
 *                       signature: '칠레 프리미엄 와인의 정수'
 *                     }
 *                   ]
 *       responses:
 *         200:
 *           description: 와인 저장 성공
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: boolean
 *               message:
 *                 type: string
 *               data:
 *                 type: array
 *                 items:
 *                   type: object
 *                   properties:
 *                     _id: 
 *                       type: string
 */ 
router.route('/write').post(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.writeWine);

/**
 * @openapi
 * paths:
 *   /api/v1/wine/delete:
 *     delete:
 *       summary: 와인 삭제 
 *       tags: [Wine]
 *       requestBody:
 *         content:
 *           application/json: 
 *             name: wine
 *             description: 'wine object'
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   _id:
 *                     type: string
 *       responses:
 *         200:
 *           description: 와인 삭제 성공
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: boolean
 *               message:
 *                 type: string
 *               data:
 *                 type: array
 *                 items:
 *                   type: object
 *                   properties:
 *                     _id: 
 *                       type: string
 */ 
router.route('/delete').delete(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.deleteWine);

/**
 * @openapi
 * paths:
 *   /api/v1/wine/upload:
 *     post:
 *       summary: 와인 사진 저장
 *       tags: [Wine]
 *       requestBody:
 *         content:
 *           multipart/form-data: 
 *             schema:
 *               type: object
 *               properties:
 *                 album: 
 *                   description: \"Wine/Photo\" or \"Wine/Label\"
 *                   default: Wine/Photo
 *                   type: string
 *                 winephoto: 
 *                   type: string
 *                   format: binary
 *
 *       responses:
 *         200:
 *           description: 와인 저장 성공
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: boolean
 *               message:
 *                 type: string
 *               data:
 *                 type: array
 *                 items:
 *                   type: object
 *                   properties:
 *                     _id: 
 *                       type: string
 */ 
router.route('/upload').post(expressJwt({secret: config.jwtSecret, algorithms: ['HS256']}), controll.uploadWinePhoto);
export default router;