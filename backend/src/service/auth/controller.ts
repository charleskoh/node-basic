import s from 'http-status';
import has from 'has-keys';

import userModel from './model';
import daouMessangerProvider from '../../provider/daou.messanger.provider';
import Token from '../../util/token';
import { KSTTokenPolicy } from '../../policy/token.policy';
import bcrypt from '../../util/bcrypt';
import { HTTP400Error } from '../../middleware/error.handler';


export default {
    async signin(req, res, next) {
        if (!has(req.body, ['email', 'password'])) {
            next(new HTTP400Error());
            return ;
        }
        let {email, password} = req.body;

        try {
            var user = await userModel.getByQuery({email: email});
            if (user && await bcrypt.compare(password, user.password)) {
                const tokenSet = Token.createToken({aud: user._id, name: user.name}, new KSTTokenPolicy());
                let data = {
                    _id: user._id,
                    name: user.name,
                    email: user.email,
                    c_id: user.c_id,
                    m_id: user.m_id,
                    c_dt: user.c_dt,
                    m_dt: user.m_dt,
                    accessToken: tokenSet.accessToken,
                    refreshToken: tokenSet.refreshToken,
                };
                res.json({status: true, message: '', data: data});
            } else {
                res.json({status: false, message: 'Can not find to user.'});
            }
        } catch (err) {
            console.log('Auth - ', err);
            next(err);
        }
    },
    async signup(req, res, next) {
        try {
            if (!has(req.body, ['email', 'password'])) {
                next( new HTTP400Error());
                return ;
            }
            let {email, password} = req.body;
            let user = await userModel.getByQuery({email: email});
            if (user) {
                res.json({status: false, message: 'exist user' });
                return ;
            }
            let enPassword = await bcrypt.encrypt(password);
            let item = {
                email: email,
                password: enPassword,
                name: req.body['name'],
                sex: req.body['sex'],
                state: req.body['state'],
                c_dt: Math.floor(Date.now() / 1000),
                m_dt: Math.floor(Date.now() / 1000),
        };
            let newUser = await userModel.create(item);
            let data = {
                _id: newUser._id,
                name: newUser.name,
                email: newUser.email,
                c_dt: newUser.c_dt,
                m_dt: newUser.m_dt,
            };
            res.json({status: true, message: '', data: data});
        } catch (err) {
            next(err);
        }
    },
}