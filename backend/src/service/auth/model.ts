
import FirebaseRealTimeDBProvider from "../../provider/firebase.database.provider";
import {ObjectId} from "mongodb"
import { isObjectBindingPattern } from 'typescript';
import MongoDBProvider from "../../provider/mongo.database.provider";

const dbo = new MongoDBProvider('auth/user');
// const dbo = new FirebaseRealTimeDBProvider('webhook');

export interface IUser {
    _id: string;
    name: string;
    email: string;
    password: string;
    c_id: string;
    m_id: string;
    c_dt: number;
    m_dt: number;
}

const user = {
    async getAll() {
        let result = await dbo.find<IUser>(0, 20, null);
        return result;
    },
    
    async getById(id) {
        let result = await dbo.find<IUser>(0, 20, {_id: id});
        return result.length > 0 ? result[0] : null;
    },

    async getByQuery(query) {
        let result = await dbo.find<IUser>(0, 20, query);
        return result.length > 0 ? result[0] : null;
    },

    async create(user) {
        return await dbo.insertOne<IUser>(user)
    },

    async update(id, user) {
        return await dbo.findOneAndUpdate<IUser>({_id: id}, {$set: user}, { returnNewDocument: true });
    },

    async delete(id) {
        return dbo.deleteOne({_id: id});
    }
}

export default user;