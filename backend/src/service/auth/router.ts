import express from "express";
import { config } from "../../middleware/config";
// import validate from 'express-validation';
import controll from "./controller"
// import validation from "./validation"
const expressJwt = require('express-jwt');

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Auth
 *   description: Auth APIs
 */

/**
 * @swagger
 * definitions:
 *   User:
 *     type: 'object'
 *     properties: 
 *       _id:
 *         type: 'string'
 *       name:
 *         type: 'string'
 *       email:
 *         type: 'string'
 *       password:
 *         type: 'string'
 *       sex: 
 *         type: 'string'
 *       state:
 *         type: 'string'
 *      
 */

/**
 * @openapi
 * paths:
 *   /api/v1/auth/signin:
 *     post:
 *       summary: 로그인
 *       tags: [Auth]
 *       requestBody:
 *         content:
 *           application/json: 
 *             name: 로그인 정보
 *             schema:
 *               type: object
 *               properties:
 *                 email: 
 *                   type: string
 *                 password: 
 *                   type: string
 *       responses:
 *         200:
 *           description: 로그인 성공
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: boolean
 *               message:
 *                 type: string
 *               data:
 *                 type: object
 *                 properties:
 *                   userInfo: 
 *                     $ref: '#/definitions/User'
 *                   tokens:
 *                     type: object
 *                     properties:
 *                       accessToken:
 *                         type: string
 *                       refreshToken:
 *                         type: string
 */ 
router.route('/signin').post(controll.signin);

/**
 * @openapi
 * paths:
 *   /api/v1/auth/signup:
 *     post:
 *       summary: 회원가입
 *       tags: [Auth]
 *       requestBody:
 *         content:
 *           application/json: 
 *             name: 가입정보
 *             schema:
 *               $ref: '#/definitions/User'
 *             examples:
 *               Charles:
 *                 value:
 *                   name: 'Charles Koh'
 *                   email: 'koh.jc@kstm.co.kr'
 *                   password: '1234'
 *                   sex: '남'
 *                   state: '부천'
 *       responses:
 *         200:
 *           description: 회원가입 성공
 *           schema:
 *             type: object
 *             properties:
 *               status:
 *                 type: boolean
 *               message:
 *                 type: string
 *               data: 
 *                 $ref: '#/definitions/User'
 */ 
 router.route('/signup').post(controll.signup);



export default router;