import MongoDBProvider from "../../provider/mongo.database.provider";

export interface IPurchaseInfo {
    _id: string;
    wine_id: string;
    store_id: string;
    user_id: string;
    type_id: string;
    price: number;
    label: string;
    receipt: string;
    comment: string;
    c_dt: string;
    m_dt: string;
    c_id: string;
}

function bindCollection<T>(db: MongoDBProvider) {
    return {
        async getAll(page = 0, size = 20, query) {
            let result = await db.find<T>(page, size, query);
            return result;
        },
        
        async getById(id) {
            let result = await db.find<T>(0, 20, {"_id": id});
            return result.length > 0 ? result[0] : {};
        },

        async create(wine) {
            return await db.insertOne<T>(wine)
        },

        async update(id, wine) {
            return await db.findOneAndUpdate<T>({"_id": id}, {$set: wine}, { returnNewDocument: true });
        },

        async delete(id) {
            return await db.deleteOne({_id: id});
        }
    };
};

const purchaseInfo = bindCollection<IPurchaseInfo>(new MongoDBProvider('WineQuery/PurchaseInfo'))

export default {
    purchaseInfo,
};