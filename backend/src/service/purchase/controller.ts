import { HTTP400Error } from "../../middleware/error.handler";
import has from 'has-keys';

import model from "./model";
import storeModel from "../store/model";
import wineModel from "../wine/model";

export default {
    async getPurchaseById(req, res, next) {
        if (!has(req.params, 'id')) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let query = {user_id: req.params['id']};
        const page = Number(req.query['page']) || 0;
        const size = Number(req.query['size']) || 20;

        let data = await model.purchaseInfo.getAll(page, size, query);
        if (!data) {
            res.json({ status: true, message: 'success', data: [] });
            return;
        }

        res.json({ status: true, message: 'success', data });
    },

    async writePurchase(req, res, next) {

        if (!has(req.body, ['wine_id', 'store_id', 'price'])) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let purchase = {
            wine_id: req.body['wine_id'],
            store_id: req.body['store_id'],
            user_id: req.body['user_id'] || req.user.aud,
            price: req.body['price'],
            label: req.body['label'],
            receipt: req.body['receipt'],
            comment: req.body['comment'],
            c_id: req.user.aud,
            m_id: req.user.aud, 
            c_dt: Date.now(),
            m_dt: Date.now(),
        }
        let info = await model.purchaseInfo.create(purchase);
        if (info) {
            res.json({ status: true, message: 'success', data: {_id: info._id.toString()} } );
            storeModel.store.updateWithPipe(purchase.store_id, {$inc:{sales_cnt:1}});
            wineModel.wine.updateWithPipe(purchase.store_id, {$inc:{sales_cnt:1}});
        } else {
            res.json({status: false, message: 'Failed to create'});
        }
    },
    
    async deletePurchase(req, res, next) {
        if (!Array.isArray(req.body)) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let validElements = req.body.filter( element => {
            return has(element, ['_id']);
        });

        if (validElements.length == 0) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        var data = [];
        for (const element of validElements) {
            let count = await model.purchaseInfo.delete(element._id);
            if (count > 0) {
                data.push(element._id);
                storeModel.store.updateWithPipe(element._id, {$inc:{sales_cnt: -1}});
                wineModel.wine.updateWithPipe(element._id, {$inc:{sales_cnt: -1}});
            }
        }

        res.json({ status: true, message: 'success', data: data});
    },
}