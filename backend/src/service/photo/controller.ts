import { HTTP400Error } from "../../middleware/error.handler";
import uploader from '../../provider/googlephoto.fileuploader.provider';
import has from 'has-keys';

import model from "./model";

export default {

    async getPhoto(req, res, next) {
        var query = {}

        const page = Number(req.query['page']) || 0;
        const size = Number(req.query['size']) || 20;

        let data = await model.photo.getAll(page, size, query);
        if (!data) {
            res.json({ status: true, message: 'success', data: [] });
            return;
        }
        res.json({ status: true, message: 'success', data });
    },
    
    async getWineById(req, res, next) {
        if (!has(req.params, 'id')) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let {id} = req.params;
        let data = await model.photo.getById(id);
        if (!data) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }
        
        res.json({ status: true, message: 'success', data });
    },

    async writeWine(req, res, next) {

        if (has(req.body, ['gphoto_id', 'rawUrl'])) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        var photo = null;
        try {
            var item =  {
                gphoto_id: req.body['gphoto_id'],
                rawUrl: req.body['rawUrl'],
                width: req.body['width'],
                height: req.body['height'],
                c_id: req.user.aud,
                m_id: req.user.aud, 
                c_dt: Date.now(),
                m_dt: Date.now(),
            }
            photo = await model.photo.create(item);
        } catch (e) {
            res.json({ status: false, message: 'Failed to Create'});
            return;
        }
        res.json({ status: true, message: 'success', data: {_id: photo._id.toString()}} );
    },
    async deleteWine(req, res, next) {
        if (!Array.isArray(req.body)) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let validElements = req.body.filter( element => {
            return has(element, ['_id']);
        });

        if (validElements.length == 0) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        var data = [];
        for (const element of validElements) {
            let count = await model.photo.delete(element._id);
            if (count) {
                data.push(element._id);
            }
        }

        res.json({ status: true, message: 'success', data: data});
    },
 }