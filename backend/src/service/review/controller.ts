import { HTTP400Error } from "../../middleware/error.handler";
import has from 'has-keys';

import model from "./model";
import wineModel from "../wine/model";

export default {
    async getReviewById(req, res, next) {
        if (!has(req.params, 'id')) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let query = {user_id: req.params['id']};
        const page = Number(req.query['page']) || 0;
        const size = Number(req.query['size']) || 20;

        let data = await model.review.getAll(page, size, query);
        if (!data) {
            res.json({ status: true, message: 'success', data: [] });
            return;
        }

        res.json({ status: true, message: 'success', data });
    },

    async writeReview(req, res, next) {

        if (!has(req.body, ['wineId', 'tannin', 'acid', 'bodied', 'sweetness', 'alcohol', 'comment'])) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let review = {
            wine_id: req.body['wineId'],
            user_id: req.user.aud,
            acid: req.body['acid'],
            tannin: req.body['tannin'],
            bodied: req.body['bodied'],
            sweetness: req.body['sweetness'],
            alcohol: req.body['alcohol'],
            comment: req.body['comment'],
            c_dt: Date.now(),
            m_dt: Date.now(),
            c_id: req.user.aud,
            m_id: req.user.aud,
        }
        let info = await model.review.create(review);
        if (info) {
            res.json({ status: true, message: 'success', data: {_id: info._id.toString()} } );
            wineModel.wine.updateWithPipe(review.wine_id, {$inc: {review_cnt: 1}});
        } else {
            res.json({ status: false, message: 'Failed to create'});
        }
    },
    
    async deleteReview(req, res, next) {
        if (!Array.isArray(req.body)) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        let validElements = req.body.filter( element => {
            return has(element, ['_id']);
        });

        if (validElements.length == 0) {
            res.json({ status: false, message: 'Bad requet'});
            return;
        }

        var data = [];
        for (const element of validElements) {
            let count = await model.review.delete(element._id);
            if (count > 0) {
                data.push(element._id);
                wineModel.wine.updateWithPipe(element._id, {$inc: {review_cnt: -1}});
            }
        }

        res.json({ status: true, message: 'success', data: data});
    },
}