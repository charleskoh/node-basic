import MongoDBProvider from "../../provider/mongo.database.provider";

export interface IReview {
    _id: string;
    user_id: string;
    wine_id: string;
    acid: number;
    tannin: number;
    bodied: number;
    sweetness: number;
    alcohol: number;
    comment: string;
    good: number;
    bad: number;
    c_dt: number;
    m_dt: number;
    c_id: string;
}

function bindCollection<T>(db: MongoDBProvider) {
    return {
        async getAll(page = 0, size = 20, query) {
            let result = await db.find<T>(page, size, query);
            return result;
        },
        
        async getById(id) {
            let result = await db.find<T>(0, 20, {"_id": id});
            return result.length > 0 ? result[0] : {};
        },

        async create(wine) {
            return await db.insertOne<T>(wine)
        },

        async update(id, wine) {
            return await db.findOneAndUpdate<T>({"_id": id}, {$set: wine}, { returnNewDocument: true });
        },

        async delete(id) {
            return await db.deleteOne({_id: id});
        }
    };
};

const review = bindCollection<IReview>(new MongoDBProvider('WineQuery/Review'))

export default {
    review,
};