import { ITokenPolicy } from "../policy/token.policy";
import { config } from '../middleware/config';

const fs = require('fs');
const jwt = require('jsonwebtoken');

export interface ITokenSet {
    accessToken: string;
    refreshToken: string;
}

export default class Token {
    static createToken(payload: any, policy: ITokenPolicy): ITokenSet {
        try {
            const _set = {
                accessToken: jwt.sign(policy.applyPolicyAccessToken(payload), config.jwtSecret, { algorithm: 'HS256'}),
                refreshToken: jwt.sign(policy.applyPolicyRefreshToken(payload), config.jwtSecret, { algorithm: 'HS256'})
            } as ITokenSet;
            return _set;
        } catch (err) {
            console.log('Token - ', err);
            throw Error ('Failed to create Token');
        }
    }

    static getPayloadFrom(token: string): any {
        try {
            return jwt.verify(token, config.jwtSecret)
        } catch (err) {
            console.log('Token - ', err);
            throw Error ('Failed to decode Token');
        }
    }

    static authenticateToken(req, res, next) {
        const authHeader = req.headers['authorization'];
        const token = authHeader && authHeader.split(' ')[1];
      
        if (token == null) return res.sendStatus(401);
      
        jwt.verify(token, config.jwtSecret as string, (err: any, user: any) => {
      
          if (err) return res.sendStatus(403);
      
          req.user = user;
      
          next();
        });
    }
}