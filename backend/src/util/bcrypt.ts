const bcrypt = require('bcrypt');
const saltRounds = 10;

export default {
    async encrypt(plainText): Promise<string> {
        return new Promise<string> ((resolve, reject) => {
            bcrypt.hash(plainText, saltRounds, function(err, hash) {
                if (err || !hash) {
                    reject(err);
                } else {
                    resolve(hash);
                }
            });
        });
    },
    async compare(plainText, hash): Promise<boolean> {
        return new Promise<boolean> ((resolve, reject) => {
            bcrypt.compare(plainText, hash, function(err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        })
    },
}